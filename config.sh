#!/bin/bash
apt update
apt install zabbix-agent -y
sed -i 's/Server\=127\.0\.0\.1/Server\=172\.16\.0\.24/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/ServerActive\=127\.0\.0\.1/Server\=172\.16\.0\.24/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/\# ListenPort\=10050/ListenPort\=10050/g' /etc/zabbix/zabbix_agentd.conf

SERVER="`hostname`"

sed -i "s/Hostname\=Zabbix\ server/Hostname\=$SERVER/g" /etc/zabbix/zabbix_agentd.conf

service zabbix-agent restart
service zabbix-agent status

echo $SERVER

ip -4 addr | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | grep 172
